# Pa11y Accessibility Testing

The .gitlab-ci.yml in this project is an example for adding pa11y automated accessibility testing to GitLab CI.


## Usage

1. Create a SSH key. 
2. Add your SSH public key in your SSH keys.
3. Create SSH_PRIVATE_KEY variable with your private SSH key value.
4. Create your feature branch and run the pipeline by each commit/push.
5. Download pa11y test artifacts from CI/CD->Pipelines


