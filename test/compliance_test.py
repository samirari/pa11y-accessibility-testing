import json 

def iscompliant():  
# Opening JSON file 
	f = open('./reports/gl-accessibility.json') 
  
	# returns JSON object as  
	# a dictionary 
	data = json.load(f) 
  
	if data['errors']==0:
		return True
	else:
		return False
	# Closing file 
	f.close()

def test_answer():
    assert iscompliant() == True
